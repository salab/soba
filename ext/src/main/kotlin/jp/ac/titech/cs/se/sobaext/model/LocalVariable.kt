package jp.ac.titech.cs.se.dependency.helper.model

import soba.core.method.LocalVariables
import kotlin.properties.Delegates

/**
 * Created by jmatsu on 2015/10/05.
 */
data class LocalVariable(val lv: LocalVariables, val index: Int) {
    val descriptor: String by lazy {
        lv.getDescriptor(index)
    }

    val type: String by lazy {
        lv.getVariableType(index)
    }

    val name: String by lazy {
        lv.getVariableName(index)
    }

    val array: Boolean by lazy {
        lv.isArrayVariable(index)
    }

    val objective: Boolean by lazy {
        lv.isObjectVariable(index)
    }

    val parameter: Boolean by lazy {
        lv.isParameter(index)
    }
}