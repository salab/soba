package jp.ac.titech.cs.se.dependency.helper

import jp.ac.titech.cs.se.sobaext.asList
import soba.core.method.DataDependence


/**
 * Created by jmatsu on 2015/10/05.
 */
fun buildDataDependency(depend: DataDependence, varName: String) {
    val index = depend.localVariables.asList().filter {
        it.name == varName
    }.firstOrNull()?.index

    if (index != null) {
        if (!depend.localVariables.hasNoDataDependence(index)) {
//            depend.getDataDefinition(index).forEach { println(it) }
            depend.edgesInSourceOrder.forEach {
                println("${depend.getVariableName(it)}")
            }
        } else {
            println("No data dependency on $varName")
        }
    }
}