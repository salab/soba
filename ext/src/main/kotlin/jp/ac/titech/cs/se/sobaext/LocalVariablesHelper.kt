package jp.ac.titech.cs.se.sobaext

import jp.ac.titech.cs.se.dependency.helper.model.LocalVariable
import soba.core.method.LocalVariables

/**
 * Created by jmatsu on 2015/10/05.
 */
fun LocalVariables.asList(): List<LocalVariable> {
    return getVariableIndexes().map { LocalVariable(this, it) }
}

private fun LocalVariables.getVariableIndexes(): List<Int> {
    return 0.rangeTo(variableEntryCount - 1).map { getVariableIndex(it) }
}