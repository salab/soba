# SOBA (Simple Objects for Bytecode Analysis)

[![Release](https://img.shields.io/github/release/jitpack/gradle-simple.svg?label=gradle)](https://jitpack.io/#org.bitbucket.salab/soba)

The perpose of this repository is only to be able to use SOBA with Maven or Gradle.

Originally hosted at [https://osdn.jp/projects/soba/](https://osdn.jp/projects/soba/).

We are NOT the authors of this product.

## Usage

# Add jitpack.io as Maven central repository.
```
repositories {
    // other repositories.  e.g. jcenter()
    maven { url "https://jitpack.io" }
}
```

# Add the dependency.

```
dependencies {
    compile "org.bitbucket.salab:soba:${release_version}"
    // Each name of published tags represents ${release_version}.
    // You can choose one of them and use it.
}
```